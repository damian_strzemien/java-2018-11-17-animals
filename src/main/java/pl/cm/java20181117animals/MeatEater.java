package pl.cm.java20181117animals;

public interface MeatEater {
    public void eatMeat();
}
