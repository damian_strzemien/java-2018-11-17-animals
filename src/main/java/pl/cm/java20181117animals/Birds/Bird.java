package pl.cm.java20181117animals.Birds;

import pl.cm.java20181117animals.Animal;

public abstract class Bird extends Animal {
    private String colorFeathers;

    public String getColorFeathers() {
        return colorFeathers;
    }

    public void setColorFeathers(String colorFeathers) {
        this.colorFeathers = colorFeathers;
    }
}
