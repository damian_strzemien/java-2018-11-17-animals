package pl.cm.java20181117animals.Birds;

import pl.cm.java20181117animals.PlantEater;

public class Parrot extends Bird implements PlantEater {

    // Nowa funkcja dla Papugi - ćwierkanie
    public void tweet() {
        System.out.println(this.getName() + " is tweeting");
    }

    // Nadpisanie metody eat z clasy Animal
    @Override
    public void eat() {

    }

    // Nadpisaanie metody eatMeat z interface PlantEater
    @Override
    public void eatPlant() {
        System.out.println(this.getName() + " is eating");
    }

}
