package pl.cm.java20181117animals.Mammals;

import pl.cm.java20181117animals.MeatEater;

public class Wolf extends Mammal implements MeatEater {

    //Dodanie funkcji wycie
    public void howl() {
        System.out.println(this.getName() + " is howlign now.");
    }

    // Nadpisanie metody eat z Animal
    @Override
    public void eat() {

    }

    // Nadpisanie metody eatMeat z interface MeatEater
    @Override
    public void eatMeat() {
        System.out.println(this.getName() + " is eating meat now.");
    }
}
