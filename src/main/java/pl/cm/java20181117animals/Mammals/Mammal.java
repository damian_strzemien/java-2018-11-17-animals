package pl.cm.java20181117animals.Mammals;

import pl.cm.java20181117animals.Animal;

public abstract class Mammal extends Animal {
    private String colorHair;

    public String getColorHair() {
        return colorHair;
    }

    public void setColorHair(String colorHair) {
        this.colorHair = colorHair;
    }
}
