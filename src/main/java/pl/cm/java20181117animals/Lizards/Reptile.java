package pl.cm.java20181117animals.Lizards;

import pl.cm.java20181117animals.MeatEater;
import pl.cm.java20181117animals.PlantEater;

public class Reptile extends Lizard implements PlantEater, MeatEater {

    // dodanie funkcji syczenie
    public void his() {
        System.out.println(this.getName() + " hissing now.");
    }

    // Nadpisanie metody eat z klasy Animal
    @Override
    public void eat() {

    }

    // Nadpisanie metody eatMeat z inferfejsu MeatEater
    @Override
    public void eatMeat() {
        System.out.println(this.getName() + " eating meat now.");
    }

    // Nadpisanie metody eatPlant z interfejsu PlantEater
    @Override
    public void eatPlant() {
        System.out.println(this.getName() + " eating plant now.");
    }

}
