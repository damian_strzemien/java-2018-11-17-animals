package pl.cm.java20181117animals.Lizards;

import pl.cm.java20181117animals.Animal;

public abstract class Lizard extends Animal {
    private String colorSkin;

    public String getColorSkin() {
        return colorSkin;
    }

    public void setColorSkin(String colorSkin) {
        this.colorSkin = colorSkin;
    }
}
