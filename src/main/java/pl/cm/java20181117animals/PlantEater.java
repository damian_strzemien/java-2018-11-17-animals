package pl.cm.java20181117animals;

public interface PlantEater {
    public void eatPlant();
}
